var express = require('express')
var compression = require('compression')
var app = express()

function shouldCompress(req, res) {
    if (req.headers['x-no-compression']) {
        // Don't compress response with this request
        return false
    }
    // Fallback to standard filter function
    return compression.filter(req, res)
}

// Setn the port of the application
// process.env.PORT lets the port be controlled
var port = process.env.PORT || 80

// Compress the content with gzip
app.use(compression({filter: shouldCompress}))

// Make express look in the dist directory for assets (css/js/img)
app.use(express.static('dist'))

app.listen(port, function() {
    console.log('Assassins Creed Interactive Map is running on http://localhost:' + port)
})