<div id="top"></div>
<!--
*** Thanks for checking out Assassins Creed Interactive Map. If you have a
*** suggestion that would make it better, please create an issue and let me know.
*** I would really appreciate it if you would star this project. Thanks again
*** and I will see you in the fight aginst the knights templar.
-->
<div align="center">
    <a href="https://gitlab.com/5pr173/assassins-creed-interactive-map">
        <img src="assets/readme/logo.png" alt="Logo" width="80" height="80">
    </a>
</div>
<br />
<div align="center">
    <h3 align="center">A free and opne source interactive map for Assassin's Creed.</h3>
    </br>
    <a href="https://app.gitbook.com/s/yEVlfb5QmsLSdi2OMyU8/"><strong>Read the docs</strong></a>
    -
    <a href="https://gitlab.com/5pr173/assassins-creed-interactive-map/-/issues"><strong>Report a bug</strong></a>
    -
    <a href="https://gitlab.com/5pr173/assassins-creed-interactive-map/-/issues"><strong>Request Features</strong>
</div>

<br />

<!-- TABLE OF CONTENTS -->
<details>
    <summary>Tabel of Contents</summary>
    <ol>
        <li>
            <a href="#about-the-project">About The Project</a>
            <ul>
                <li><a href="#built-with">Built With</a></li>
            </ul>
        </li>
        <li>
            <a href="#getting-started">Getting Started</a>
            <ul>
                <li><a href="#prerequisites">Prerequisites</a></li>
                <li><a href="#installation">Installation</a></li>
            </ul>
        </li>
        <li><a href="#usage">Usage</a></li>
        <li><a href="#supportedbrowsers">Supported Browsers</a></li>
        <li><a href="#contributing">Contributing</a></li>
        <li><a href="#license">License</a></li>
        <li><a href="#contact">Contact</a></li>
        <li><a href="#acknowledgments">Acknowledgments</a></li>
    </ol>
</details>

<!-- About the project -->
## About

<div align="center">
    <img src="" alt="map">
</div>

Interactive maps for assassins creed do exist so this project is nothing new so that may leave you to wonder why did I create this map. I created this map because the best Assassin's Creed interactive map out there which can me found on <a href="https://mapgenie.io/">mapgenie.io</a> but that map comes with restrictions if you don't have a pro plan which cost $5.00 per map or $10.00 a year and their maps are closed source. If your like me and can't afford to use map genie then you have to look for alternatives which are not alwasy avaiable. That is why I decided to create this map which is not only completely free to use but it is also open source and fully documented.

This map contains all of the locations, collectibles, and missions for Assassin's Creed. If I missed something please create an issue or open a ticket on the offical Discord server.

### Built With
This entire project was built using nothing but HTML, CSS and Javascript. For a code editor I used Visual Studio Code.

## Getting Started

If you want to get a copy of this project running on your computer locally for development or to develope your own make then follow these instructions. If not they you can disregard this section.

### Prerequisites
The prerequisites for this project are pretty simple. You will need the following things to get started:
- A code editor(I suggest VSCode)

If you plan on hosting it you will need a server to host it on. There are instructions on how to host it on Gitlab further down.

### Installation

To get started with development all you need to do is download this project and index.html file. Hosting instructions are further down.

If you have any questions please feel free to open up a ticket or head on over to my Discord server.

## Supported Browsers
Assassin's Creed Interactive Map is currently supported on the following browsers. The map will probably work on any browser but I won't help with issues due to trying it on a browser I haven't confirmed the map works on. 

![Brave](https://img.shields.io/badge/Brave-FB542B?style=for-the-badge&logo=Brave&logoColor=white)

## Roadmap
- [X] Confirm app runs on Brave
- [ ] Confirm app runs on Firefox
- [ ] Confirm app runs on Edge
- [ ] Confirm app runs on Google Chrome
- [ ] Confirm app runs on Tor
- [ ] Develope native Windows App
- [ ] Develope native Linux App
- [ ] Develope native Android App

## Contributing
To contribute towards this project please fork the repo and then create a pull request with your changes.

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

## Contact

5pr173
- [Email](5rp173@protonmail.com)
- [Twitter]()
- [Discord Server]()
